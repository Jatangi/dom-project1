
let count = 0

let globalallRadios = document.getElementsByName('radio')

var inputData = document.getElementById('input')
//list of added items
var listitems = document.getElementById('list-items')
//is radio button checked
var radiobutton = document.getElementById("radio-btn")

var allListItems = document.getElementById('all')

var completedListItems = document.getElementById('completed')

var activelistItems = document.getElementById('active')

var clearCompleteditems = document.getElementById('clear-completed')

var eachli = document.getElementById('li-div')

var checkall = document.getElementById('all')

let isChecked = false



//input data
inputData.addEventListener('keypress', addInput)

//delete list item
listitems.addEventListener('click', deleteitem)
//checking radio button
//radiobutton.addEventListener('click',radioCheck)
clearCompleteditems.addEventListener('click', clearCompleted)

allListItems.addEventListener('click', displayAll)

completedListItems.addEventListener('click', displaycompleted)

activelistItems.addEventListener('click', displayactive)

listitems.addEventListener('click', striketext)

listitems.addEventListener('dblclick', editenable)

checkall.addEventListener('click', markchecked)
function markchecked() {
    isChecked=!isChecked
    let checkboxes = document.querySelectorAll("input[type='checkbox']")
    checkboxes.forEach(function (checkbox) {
        let labelText=checkbox.parentElement.querySelector('#label-data')
        if(isChecked){
            checkbox.checked = true;
            labelText.innerHTML = `<del>${labelText.textContent}</del>`
            labelText.style.color = 'grey'
        }else{
            checkbox.checked = false;
            labelText.innerHTML = `${labelText.textContent}`
            labelText.style.color = 'black'
        }
        
    });
    count=isChecked?0:listitems.children.length;
    countlistitems(count)
}



function editenable(e) {
    const label = listitems.querySelector('label')
    const checkbox = listitems.querySelector('input')
    const button = listitems.querySelector('button')

    let currentText = label.textContent
    let input = document.createElement('input')
    input.type = "text"
    input.value = currentText

    label.parentNode.replaceChild(input, label)

    input.focus()
    input.className = 'p-6 w-full'
    checkbox.classList.add('hidden')
    button.classList.add('hidden')

    input.addEventListener('keypress', function (event) {

        if (event.key === "Enter") {
            label.textContent = input.value

            input.parentNode.replaceChild(label, input)
            checkbox.classList.remove('hidden')
            button.classList.remove('hidden')

        }
    })


    input.addEventListener('blur', function () {

        label.textContent = label.textContent
        input.parentNode.replaceChild(label, input)
        checkbox.classList.remove('hidden')
        button.classList.remove('hidden')

    })

}




document.getElementById('menu-bar').style.visibility = 'hidden'


function edithead() {

    const newh = document.createElement('h1')
    newh.textContent = "thanu"
    testheader.replaceWith(newh)
    console.log(testheader.textContent)
}

function addInput(e) {

    //e.preventDefault()
    console.log(e.target.value)

    if (e.key === "Enter") {
        document.getElementById('menu-bar').style.visibility = 'visible'

        var newItem = e.target.value

        //add item

        const saveinput = document.createElement('input')
        input.focus()

        saveinput.type = 'checkbox'

        saveinput.id = "radio-btn"

        saveinput.name = 'radio'

        
        saveinput.className = 'appearance-none w-8 h-8 border  py-3 rounded-full text-green-200 checked:bg-green-50 checked:border-green focus:outline-none'

        const label = document.createElement('label')

        label.id = "label-data"

        label.className = "text-left text-xl ml-10 py-1"

        label.textContent = newItem

        const delbtn = document.createElement("button")

        delbtn.id = "del-btn"

        delbtn.className = " delete float-right"
        //delbtn.style= "float:right; margin-right:3px"

        delbtn.textContent = 'x'

        var newdiv = document.createElement('div')

        newdiv.id = "li-div"

        newdiv.className = 'pl-5 pr-10  bg-white'


        newdiv.appendChild(saveinput)
        newdiv.appendChild(label)
        newdiv.appendChild(delbtn)


        const list = document.createElement('li')
        list.id = 'each-li'
        list.className = "pt-3 pb-3 border-0 w-[490px] border-2"
        list.appendChild(newdiv)

        document.getElementById('list-items').appendChild(list)
       
        count += 1

        countlistitems(count)


        e.target.value = ""



    }

}
//delete item

function deleteitem(e) {
    if (e.target.id == 'del-btn') {

        const deleteli = e.target.parentElement
        
        deleteli.parentElement.remove()
        
        count--
        countlistitems(count)
    }
}

function countlistitems(count){
    // = document.querySelectorAll('input[type="checkbox"]:checked').length;
    document.getElementById('list-count').textContent = `${count} items left!`;
}

function displaycompleted() {
    let allRadios = document.getElementsByName('radio')

    for (let index = 0; index < allRadios.length; index++) {

        if (allRadios[index].checked == true) {

            allRadios[index].parentElement.parentElement.style.display = 'block'

        }
        else {
            allRadios[index].parentElement.parentElement.style.display = 'none'
        }
    }


}

function displayAll() {
    let allRadios = document.getElementsByName('radio')
    for (let index = 0; index < allRadios.length; index++) {


        allRadios[index].parentElement.parentElement.style.display = 'block'

    }
}

function displayactive() {
    //console.log(listitems)

    let allRadios = document.getElementsByName('radio')

    for (let index = 0; index < allRadios.length; index++) {

        if (allRadios[index].checked == false) {

            allRadios[index].parentElement.parentElement.style.display = 'block'

        }
        else {
            allRadios[index].parentElement.parentElement.style.display = 'none'
        }
    }
}

function striketext(e) {
    const parentnode = e.target.parentNode
    const labelText = parentnode.querySelector('#label-data')

    if (e.target.checked) {
        count -= 1
        labelText.innerHTML = `<del>${labelText.textContent}</del>`
        labelText.style.color = 'grey'
    }
    else {
        count += 1
        labelText.innerHTML = `${labelText.textContent}`
        labelText.style.color = 'black'


    }
    countlistitems(count)
}

function clearCompleted() {
    let allRadios = document.getElementsByName('radio')



    for (let index = allRadios.length - 1; index >= 0; index--) {

        if (allRadios[index].checked === true) {
            allRadios[index].parentElement.parentElement.remove()


        }

    }
}



















